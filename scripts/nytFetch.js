var newURL;
const BASE_URL = 'https://api.nytimes.com/svc/books/v3/lists/names.json?api-key=';

var url = BASE_URL+API_KEY;

fetch(url)
  .then(function(response) {
    return response.json();
  })
  .then(function(responseJson) {

     for (let i=0; i < responseJson.num_results; i++)
     {
      var newType = responseJson.results[i].display_name;
      var newTypeValue = responseJson.results[i].list_name_encoded;
      $('#book-type').append('<option value=' + newTypeValue + '>' + newType + '</option>');
     }
    });

    $('#list-submit').on('click', function(){

      selectedVal = $('#book-type').val();
      selectedDate = $('#list-date').val();

      if (!selectedDate)
      {
        selectedDate = 'current'
      }
    const BASE_URL2 = 'https://api.nytimes.com/svc/books/v3/lists/';
    newURL = BASE_URL2 +selectedDate+ '/' +selectedVal+ '.json?api-key='+API_KEY

    fetch(newURL)
      .then(function(response) {
        return response.json();
      })
      .then(function(responseJson){
      let article = responseJson.results;
      $( "#best-sellers" ).empty();
        //clear div best-sellers
      for (let i=0; i < 5; i++)  //article.books.length
      {
       var newBookName = article.books[i].title;
       var newISBN = article.books[i].primary_isbn13;
       var newBookAuthor = article.books[i].author;
       var newBookDesc = article.books[i].description;
       var newBookImg = article.books[i].book_image;

       $('#best-sellers').append('<li id="' +newISBN+ '"><img src="' +newBookImg+ '"></li><br/>');
       $('#' +newISBN+ '').append('<ul><li>' +newBookName+ '</li><li>' +newBookAuthor+ '</li><li>' +newBookDesc+ '</li></ul><br/>')
      }
    })
});
  
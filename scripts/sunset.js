let x = 0;
let y = 255;

const sunset = function() 
{
    x++;
    y--;

    if ( x < 255)  
    {
    document.body.style.backgroundColor = `rgb(${x},${x},${x})`;
    document.body.style.color = `rgb(${y},${y},${y})`;

    requestAnimationFrame(sunset);
    }
}    
    requestAnimationFrame(sunset);
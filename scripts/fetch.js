const BASE_URL = 'https://api.nytimes.com/svc/search/v2/articlesearch.json';

const url = `${BASE_URL}?q=cars&api-key=${API_KEY}`;

fetch(url)
  .then(function(response) {
    return response.json();
  })
  .then(function(responseJson) {

    let article = responseJson.response.docs[9];
    console.log(article);

    const mainHeadline = article.headline.main;
    document.getElementById('article-title').innerText = mainHeadline;

    const byLine = article.byline.original;
    document.getElementById('article-byline').innerText = byLine;

    const fullArticle = article.web_url;
    document.getElementById('article-link').setAttribute('href', fullArticle);

    const snippet = article.snippet;
    document.getElementById('article-snippet').innerHTML = snippet;
    
    if (article.multimedia.length > 0) {
      const imgUrl = `https://www.nytimes.com/${article.multimedia[0].url}`;
      document.getElementById('article-img').src = imgUrl;
    } else 
    {
      document.getElementById('article-img').setAttribute('src', 'car.png')
    }
  });